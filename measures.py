#!/usr/bin/env python

import logging

logger = logging.getLogger(__name__)

def gini_score(D):
    """
        gini(D) = 1 - SUM(P(d)**2)
    """
    score = 1 - D.div(D.sum().sum()).sum().apply(lambda x: x**2).sum()
    logger.debug("%s gini_score=%s" % (D.index, score))
    return score

def gainInformation(D):
    sum_probabilities = D2.div(D2.sum().sum(), axis=0).sum().apply(lambda x: x * math.log(x, 2)).sum()
    return sum_probabilities * -1
