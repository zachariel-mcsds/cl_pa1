#!/usr/bin/env python
import logging

logger = logging.getLogger(__name__)

class Node:
    pass

class DecisionNode(Node):
    def __init__(self, attribute, score, child_options):
        self.attribute = attribute
        self.score = score
        self.child_options = list(child_options)
        self.childs = dict()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '<DecisionNode attribute=%s childs=%s' % (self.attribute, list(self.child_options))

    def addChild(self, attribute, node):
        logger.info("Node created %s" % node)
        attribute = frozenset(attribute)
        self.childs[attribute] = node
        return self

class LeafNode(Node):
    def __init__(self, classLabel):
        self.class_label = classLabel

    def __repr__(self):
        return "<LeafNode label='%s'>" % self.class_label

    def __str__(self):
        return "<LeafNode label='%s'>" % self.class_label

class Tree:
    def __init_(self, root):
        self.root = root;
        pass
