#!/usr/bin/env python

import logging
import measures
import pandas as pd
from feature import Feature

logger = logging.getLogger(__name__)

class AttributeSelector:
    def __init__(self, D, target_column="target"):
        self.D = D
        self.target_column = target_column
        logger.debug("AttributeSelector: %s" % [self.D.shape])

        summary = self.D.groupby(self.target_column).size()
        d = pd.DataFrame([list(summary)], columns=list(summary.index))
        self.giniD = measures.gini_score(d)
        logger.debug("Subset GiniD = %s" % self.giniD)

    def choose(self):
        #for name, column in D:
        #    partition = Partition(D[[name, self.target_column]])

        best_score = 0
        best_attribute = None
        for name, column in self.D.iteritems():
            if name == self.target_column:
                continue
            feature = Feature(self.D[[name, self.target_column]], name)
            best_split = feature.gini_split()

            if best_split is None:
                logger.debug("best_split is None")
                return None

            if best_split[1] is None:
                logger.debug("best_split score is None")
                return None

            if best_split[0] is 0:
                logger.debug("best_split attribute is None")
                return None

            if best_attribute is None:
                best_score = best_split[1]/self.giniD
                best_attribute = best_split

            if (best_attribute[1]/self.giniD) > best_score:
                best_score = best_split[1]/self.giniD
                best_attribute = best_split

        return best_attribute
