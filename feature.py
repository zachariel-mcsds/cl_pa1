#!/usr/bin/env python
import logging
import pandas as pd
import itertools

import measures

logger = logging.getLogger(__name__)

class PartitionException(Exception):
    pass

class Feature:
    def __init__(self, D, feature_name, target_column="target"):
        self.D = D
        if self.D.empty:
            raise Exception('Trying to get feature score from empty dataset')

        self.feature_name = feature_name
        self.target_column = target_column
        self.feature_values = D[feature_name].unique()

        self._summary = None

        logger.debug("Feature %s" % (feature_name))
        logger.debug("Feature values = " % (self.feature_values))

    @property
    def name(self):
        return self.feature_name

    @property
    def summary(self):
        if( self._summary is None):
            grouped = self.D.groupby(list(self.D.columns)).size()
            counters = grouped.values.reshape(len(grouped.index.levels[0]), len(grouped.index.levels[1]))
            self._summary = pd.DataFrame(counters,
                                         index=list(grouped.index.levels[0]),
                                         columns=list(grouped.index.levels[1]))

            #self._summary = pd.DataFrame(columns=self.D.target.unique(), index=self.D[self.name].unique())
            #for name, group in self.D.groupby([self.name, self.target_column]):
            #    self._summary.at[name] = len(group)

        return self._summary

    @property
    def gini(self):
        return measures.gini_score(self.summary)

    @property
    def total(self):
        return self.summary.sum().sum()

    @property
    def options(self):
        return self.D[self.name].unique()

    def single_class(self):
        return len(self.D[self.target_column].unique()) == 1

    def binary_split(self):
        logger.debug('Binary partition %s => %s' % (self.options, self.D[self.target_column].unique()))
        score = 0
        for attr in self.options:
            split = self.summary.loc[attr]
            split = pd.DataFrame([split], columns=self.summary.columns)
            score += (split.sum().sum()/self.total) * measures.gini_score(split)
            #score += (self.summary.sum().sum()/self.total) * measures.gini_score(self.summary)

        return (self.name, score, [frozenset([x]) for x in self.options])

    def partition_split(self):
        logger.debug("Finding best subset to split")
        best_score = None
        best_partition = None;

        for idx, partition in enumerate(self.partitions):
            #logger.debug("Calculating partition score %s" % partition)
            partition_score = 0

            for i, elements in enumerate(partition):
                split = self.summary.loc[elements]
                partition_score += (split.sum().sum()/self.total) * measures.gini_score(split)
            #else:
            #    split = self.summary.loc[partition]
            #    partition_score += (split.sum().sum()/self.total) * measures.gini_score(split)

            #logger.debug("Partition score = %s" % partition_score)

            if best_score is None:
                best_score = partition_score
                best_partition = partition
            else:
                if partition_score < best_score:
                    best_score = partition_score
                    best_partition = partition

        logger.debug("Best Partition = %s" % ([self.name, best_score, best_partition]))
        return (self.name, best_score, best_partition)

    def gini_split(self):
        """
            gini_tuple(D) = SUM-i1-to-n( D1/D * gini(D1) )
        """
        if self.single_class():
            logger.debug('Single class %s' % self.D[self.target_column].unique())
            raise Exception('Single class detected!')

            return ( self.name, measures.gini_score(self.summary), self.D[self.name].unique() )

        if len(self.options) == 1:
            logger.debug('Cannot be partitioned %s => %s' % (self.D[self.name].unique(), self.D[self.target_column].unique()))
            logger.debug('Majority selection %s', self.D.groupby([self.name, self.target_column]).size())
            return (self.name, measures.gini_score(self.summary), self.D[self.name].unique() )

        if len(self.options) == 2:
            return self.binary_split()
        else:
            return self.partition_split()


    @property
    def partitions(self):
        if len(self.feature_values) < 3:
            division_candidates = frozenset([frozenset([x]) for x in self.feature_values])
        else:
            combinations = itertools.combinations(self.feature_values, 2)
            division_candidates = set()
            for combination in combinations:
                tuple_value = frozenset([frozenset(combination), frozenset(self.feature_values).difference(combination)])
                division_candidates.add(tuple_value)

        return division_candidates

