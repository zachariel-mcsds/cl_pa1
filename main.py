#!/usr/bin/env python

from decision_tree_classifier import DecisionTreeClassifier
import pandas as pd
import numpy as np
import logging
import os

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(module)s %(funcName)s %(message)s')
logger = logging.getLogger(__name__)

def file_content(filename='data/places.txt', separator=' ', line_processor=None):
    """Read file and yield line by line"""
    root_dir = (os.path.dirname(os.path.realpath(__file__)))
    file_path = root_dir + filename
    logger.debug("Reading %s" % file_path)

    with open(file_path, 'r') as f:
        for line in f:
            line = line.rstrip()
            yield line

def testing_dataset():
    # <= 30   == 1
    # 31...40 == 2
    # > 40    == 3
    columns = ['age', 'income', 'student', 'credit_rating', 'target']
    itemset = [
        ['youth', 'high', 'no', 'fair', 'no'],
        ['youth', 'high', 'no', 'excellent', 'no'],
        ['middle_aged', 'high', 'no', 'fair', 'yes'],
        ['senior', 'medium', 'no', 'fair', 'yes'],
        ['senior', 'low', 'yes', 'fair', 'yes'],
        ['senior', 'low', 'yes', 'excellent', 'no'],
        ['middle_aged', 'low', 'yes', 'excellent', 'yes'],
        ['youth', 'medium', 'no', 'fair', 'no'],
        ['youth', 'low', 'yes', 'fair', 'yes'],
        ['senior', 'medium', 'yes', 'fair', 'yes'],
        ['youth', 'medium', 'yes', 'excellent', 'yes'],
        ['middle_aged', 'medium', 'no', 'excellent', 'yes'],
        ['middle_aged', 'high', 'yes', 'fair', 'yes'],
        ['senior', 'medium', 'no', 'excellent', 'no']
    ]

    dataset = pd.DataFrame(itemset, columns=columns)
    return dataset

def testing_data():
    try:
        testing = pd.read_csv('data/testing.csv')
        logger.debug('Testing loaded from CSV')
    except:
        logger.debug('Testing loaded from txt')
        testing = pd.DataFrame()
        for line in file_content('/data/testing.txt'):
            attributes = process_values(line)
            row = pd.DataFrame(data=[attributes[:,1]], columns=attributes[:, 0])
            testing = testing.append(row)

        testing.to_csv('data/testing.csv', index=False)
        logger.debug('Testing saved to CSV')

    return testing.reset_index()

def training_data():
    try:
        training = pd.read_csv('data/training.csv')
        logger.debug('Training loaded from CSV')
    except:
        logger.debug('Training loaded from txt')
        training = pd.DataFrame()
        for line in file_content('/data/training.txt'):
            row = process_line(line)
            training = training.append(row, ignore_index=True)

        training.to_csv('data/training.csv', index=False)
        logger.debug('Training saved to CSV')

    return training

def process_line(line):
    separator=' '
    line = line.rstrip()
    class_label, values = line.split(separator, 1)

    pairs = process_values(values)
    attributes = np.append(pairs, [['target', ("C%s" % class_label)]]).reshape(-1, 2)

    return pd.DataFrame(data=[attributes[:,1]], columns=attributes[:, 0])

def process_values(values_str):
    values = np.array([ [("A%s"%column), ("a%s_%s" % (column, value))] for column, value in [pair.split(':') for pair in values_str.split(' ')] ])
    return values

if __name__ == '__main__':
    logger.info('MCS-DS/cs412/cl_pa1')
    training = training_data()
    testing  = testing_data()

    logger.debug(training.shape)
    logger.debug(testing.shape)

    tree = DecisionTreeClassifier(training)
    tree.train()
    tree.test()
    tree.predict()
    print( tree.stats() )

