#!/usr/bin/env python
import logging
import itertools
import pandas as pd
import measures
import decision_tree as dt

from attribute_selector import AttributeSelector

logger = logging.getLogger(__name__)

def test(root, D):
    logger.info('Testing Decision Tree')
    counter = 0
    for idx, row in D.iterrows():
        r = pd.DataFrame([list(row)], columns=D.columns)
        class_label = choose_node(root, r.reset_index())

        try:
            if class_label.class_label == r['target'][0]:
                counter += 1
        except:
            logger.debug("Couldn't get class")

    return counter

def choose_node(node, obj):
    #print(node)
    v = obj[node.attribute][0]
    for condition, child in node.childs.items():
        #print(condition)
        if v in condition:
            if type(child) is dt.DecisionNode:
                return choose_node(child, obj)
            else:
                #print(child)
                return child
        #else:
        #    return 'not in condition'
        #    print(node)
    return node

def iterate_node(node):
    if type(node) is dt.LeafNode:
        print("Leaf %s" % node.class_label)
    else:
        for condition, child in node.childs.items():
            print("Feature = %s" % node.attribute)
            print("Condition = %s" % condition)
            iterate_node(child)

class DecisionTreeClassifier:
    def __init__(self, training, min_support=30, target_column="target"):
        logger.info('Decision Tree')
        self.training = training
        self.target_column = target_column
        self.level = -1
        self.min_support = min_support
        self.tree = None

    @property
    def labels(self):
        return self.training[self.target_column].unique()

    def find_best_attribute_to_split(self, D):
        """
        for each column in dataset, calculate best gini index,
        to calculate it, we need to create tuples of values,
        """

        return AttributeSelector(D).choose()

    def create_tree(self):
        pass

    def create_node(self):
        pass

    def create_leaf(self):
        pass

    def generate(self, D):
        """
        - create_node()
        - if len(D['target'].unique()) == 1:
        -    return N as leaf node labeled with class D['target'].unique()[0]
        - if len(attribute_list) == 0:
        -    return N as leaf node labeled with the majority class in D; //majority voting
        - apply attribute_selection(D, attribute_list) to find "best" splitting_attribute/splitting_criterion
        - label node N with splitting_attribute
        - if splitting_attribute is discrete-valued
        -    attribute_list = attribute_list - splitting_attribute
        - for criteria in splitting_criterion:
        - Dj = D[criteria]
        - if len(Dj) == 0:
        - 
        """

        #giniD = measures.gini_score(D)
        if len(D.columns) == 1:
            raise Exception('No more attributes, just target column')

        if len( D[self.target_column].unique() ) == 1:
            return dt.LeafNode( D[self.target_column].unique()[0] )

        #if type( split_attribute ) is dt.LeafNode:
        #    return split_attribute

        split_attribute = self.find_best_attribute_to_split(D)

        if len( D[split_attribute[0]].unique() ) == 1:
            return dt.LeafNode( D.groupby(self.target_column).size().idxmax() )
            raise Exception('Attribute with just one value')
            return dt.LeafNode( D[self.target_column].unique()[0] )

        if len( D[split_attribute[0]] ) <= self.min_support:
            return dt.LeafNode( D.groupby(self.target_column).size().idxmax() )


        try:
            if( split_attribute is None ):
                logger.info('---> Leaf node %s' % ([split_attribute]))
                raise Exception('Attribute with just one value')
                return None
        except:
            logger.debug('Exception %s' % split_attribute)

        node = dt.DecisionNode(*split_attribute)
        self.level += 1
        logger.info("Creating branches for decision node %s shape %s" % (node, D.shape))
        for attribute in node.child_options:
            logger.debug("Creating subset for %s => %s" % (node.attribute, [attribute]))

            if type(attribute) is str:
                attribute = list([attribute])

            Dpartition = D[D[node.attribute].isin(list(attribute))].drop(columns=node.attribute)

            logger.info( "Feature %s shape %s" % (attribute, Dpartition.shape) )
            if len(Dpartition.columns) == 1:
                if len(Dpartition[self.target_column].unique()) == 1:
                    child = dt.LeafNode( Dpartition[self.target_column].unique()[0] )
                else:
                    max_decision = Dpartition.max()[self.target_column]
                    child = dt.LeafNode( max_decision )
            else:
                logger.debug("Subset %s" % [Dpartition.shape])
                if Dpartition.empty:
                    print('Empty partition')
                    print(D.head())
                    print(attribute)
                child = self.generate(Dpartition)
                #if type(child) is dt.DecisionNode:
                #    child.setValue(attribute)

            node.addChild(attribute, child)

        return node

    def train(self):
        logger.info('Training Decision Tree')

        root = self.generate(self.training)
        #self.tree = dt.Tree(root)
        return root

    def predict(self):
        logger.debug('Predict value')
        pass

    def stats(self):
        pass

